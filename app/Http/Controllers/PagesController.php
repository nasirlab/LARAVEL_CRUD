<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
class PagesController extends Controller
{
    public function index(){

    	if(View::exists('pages.index'))
    		return view('pages.index')
    			->with('text','<h1>Data from Pages controller</h1>')
    			->with('name','Md Nasir Fardoush');
    	else
    		return 'No view available';
    }

    public function profile()
    {
    	return view('pages.profile');
    	
    }
    public function settings()
    {
        return view('pages.settings');
    }

    public function blade()
    {
        return view('blade.bladetest');
    }
}
